from flask import Flask, render_template, request, jsonify

app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/case-cliquee', methods=['POST'])
def case_cliquee_post():
    a = request.json
    print(a)
    return jsonify(a)


@app.route('/case-cliquee')
def case_cliquee():
    a = request.args.get('id')
    b = request.args.get('nom')
    print(b)
    return 123


if __name__ == '__main__':
    app.run(debug=True)
