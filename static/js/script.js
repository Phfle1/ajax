'use strict'

$(document).ready(function () {

    const jeu = document.getElementById('jeu')
    const table = document.createElement('table')
    for (let i = 0; i < 8; i++) {
        const rangee = document.createElement('tr')
        for (let j = 0; j < 8; j++) {
            const laCase = document.createElement('td')
            laCase.style.height = '50px'
            laCase.style.minWidth = '50px'
            laCase.setAttribute('data-num', i * 8 + j)
            if ((j + i) % 2 === 0) {
                laCase.style.backgroundColor = 'black'
            }
            rangee.appendChild(laCase)
        }
        table.appendChild(rangee)
    }
    jeu.appendChild(table)

    /* notez que j'ajoute l'évènement sur le jeu seulement. On pourrait
    l'ajouter sur toutes les cases et ça fonctionnerait aussi, mais ce serait plus gourmand en mémoire */
    jeu.addEventListener('click', afficherCaseCliquee)
})


function afficherCaseCliquee(event) {
    const p = document.getElementById('case-cliquee')
    p.innerHTML = '' // On pourrait aussi utiliser removechild
    const num = event.target.getAttribute('data-num')
    const text = document.createTextNode(num)
    p.appendChild(text)
    let donnees = {numero:[1, 2, 3]}
    const id_entite = 1
    //Requête get
    $.get({
        url:'/case-cliquee', // où on fait la requête
        data: {id:id_entite, nom:'philipppe'}, // les données
        contentType: "application/json", // Le format de l'envoi
        dataType: 'json', // ce qui sera reçu
        success: function(donnees, status, jqXHR){
            alert(jqXHR)
        },
        error: function(jqXHR, statut, erreur){
            alert(erreur)
        }
    })
    //Requête post
    $.post({
        url:'/case-cliquee', // où on fait la requête
        data: JSON.stringify(donnees), // les données
        contentType: "application/json", // Le format de l'envoi
        dataType: 'json', // ce qui sera reçu
        success: function(donnees, status, jqXHR){
            alert(donnees)
        },
        error: function(jqXHR, statut, erreur){
            alert(erreur)
        }
    })
}

